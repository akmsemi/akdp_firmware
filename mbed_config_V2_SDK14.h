/*
 * mbed SDK
 * Copyright (c) 2017 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Automatically generated configuration file.
// DO NOT EDIT, content will be overwritten.

#ifndef __MBED_CONFIG_DATA__
#define __MBED_CONFIG_DATA__

// Configuration parameters
#define MBED_CONF_NORDIC_UART_DMA_SIZE                     8                                                                                                // set by library:nordic
#define MBED_CONF_LORA_DEVICE_ADDRESS                      0x00000000                                                                                       // set by library:lora
#define MBED_CONF_LORA_AUTOMATIC_UPLINK_MESSAGE            1                                                                                                // set by library:lora
#define MBED_CONF_EVENTS_SHARED_STACKSIZE                  1024                                                                                             // set by library:events
#define MBED_CONF_LORA_DUTY_CYCLE_ON                       1                                                                                                // set by library:lora
#define MBED_CONF_LORA_DEVICE_EUI                          {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}                                                 // set by library:lora
#define MBED_CONF_EVENTS_SHARED_DISPATCH_FROM_APPLICATION  0                                                                                                // set by library:events
#define MBED_CONF_PPP_CELL_IFACE_BAUD_RATE                 115200                                                                                           // set by library:ppp-cell-iface
#define MBED_CONF_PPP_CELL_IFACE_APN_LOOKUP                1                                                                                                // set by library:ppp-cell-iface
#define MBED_CONF_EVENTS_PRESENT                           1                                                                                                // set by library:events
#define MBED_CONF_NORDIC_NRF_LF_CLOCK_CALIB_MODE_CONFIG    0                                                                                                // set by target:MCU_NRF52832
#define MBED_CONF_LORA_NB_TRIALS                           12                                                                                               // set by library:lora
#define MBED_CONF_LORA_NWKSKEY                             {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00} // set by library:lora
#define MBED_LFS_PROG_SIZE                                 64                                                                                               // set by library:littlefs
#define MBED_CONF_LORA_PHY                                 EU868                                                                                            // set by library:lora
#define MBED_CONF_PLATFORM_STDIO_FLUSH_AT_EXIT             1                                                                                                // set by library:platform
#define MBED_CONF_NORDIC_UART_1_FIFO_SIZE                  32                                                                                               // set by library:nordic
#define MBED_CONF_DRIVERS_UART_SERIAL_RXBUF_SIZE           256                                                                                              // set by library:drivers
#define NVSTORE_MAX_KEYS                                   16                                                                                               // set by library:nvstore
#define MBED_CONF_NSAPI_PRESENT                            1                                                                                                // set by library:nsapi
#define MBED_CONF_FILESYSTEM_PRESENT                       1                                                                                                // set by library:filesystem
#define MBED_CONF_CELLULAR_USE_APN_LOOKUP                  1                                                                                                // set by library:cellular
#define MBED_LFS_BLOCK_SIZE                                512                                                                                              // set by library:littlefs
#define MBED_CONF_PLATFORM_POLL_USE_LOWPOWER_TIMER         0                                                                                                // set by library:platform
#define MBED_CONF_PLATFORM_FORCE_NON_COPYABLE_ERROR        0                                                                                                // set by library:platform
#define MBED_CONF_LORA_LBT_ON                              0                                                                                                // set by library:lora
#define MBED_CONF_PLATFORM_STDIO_BAUD_RATE                 9600                                                                                             // set by library:platform
#define MBED_CONF_PPP_CELL_IFACE_AT_PARSER_BUFFER_SIZE     256                                                                                              // set by library:ppp-cell-iface
#define MBED_CONF_PLATFORM_STDIO_BUFFERED_SERIAL           0                                                                                                // set by library:platform
#define MBED_LFS_INTRINSICS                                1                                                                                                // set by library:littlefs
#define MBED_CONF_EVENTS_SHARED_HIGHPRIO_STACKSIZE         1024                                                                                             // set by library:events
#define NVSTORE_ENABLED                                    1                                                                                                // set by library:nvstore
#define MBED_CONF_LORA_TX_MAX_SIZE                         64                                                                                               // set by library:lora
#define MBED_CONF_LORA_ADR_ON                              1                                                                                                // set by library:lora
#define MBED_LFS_READ_SIZE                                 64                                                                                               // set by library:littlefs
#define MBED_CONF_PLATFORM_DEFAULT_SERIAL_BAUD_RATE        9600                                                                                             // set by library:platform
#define MBED_CONF_RTOS_PRESENT                             1                                                                                                // set by library:rtos
#define MBED_CONF_EVENTS_SHARED_EVENTSIZE                  256                                                                                              // set by library:events
#define MBED_CONF_LORA_APP_PORT                            15                                                                                               // set by library:lora
#define MBED_CONF_PLATFORM_STDIO_CONVERT_TTY_NEWLINES      0                                                                                                // set by library:platform
#define MBED_CONF_CELLULAR_RANDOM_MAX_START_DELAY          0                                                                                                // set by library:cellular
#define MBED_CONF_PPP_CELL_IFACE_AT_PARSER_TIMEOUT         8000                                                                                             // set by library:ppp-cell-iface
#define MBED_CONF_LORA_APPSKEY                             {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00} // set by library:lora
#define MBED_CONF_LORA_APPLICATION_EUI                     {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}                                                 // set by library:lora
#define MBED_CONF_NORDIC_NRF_LF_CLOCK_SRC                  NRF_LF_SRC_XTAL                                                                                  // set by target:MCU_NRF52832
#define MBED_CONF_DRIVERS_UART_SERIAL_TXBUF_SIZE           256                                                                                              // set by library:drivers
#define MBED_CONF_TARGET_CONSOLE_UART_FLOW_CONTROL         RTSCTS                                                                                           // set by library:nordic[NRF52_DK]
#define MBED_LFS_LOOKAHEAD                                 512                                                                                              // set by library:littlefs
#define MBED_CONF_EVENTS_USE_LOWPOWER_TIMER_TICKER         0                                                                                                // set by library:events
#define MBED_CONF_LORA_APPLICATION_KEY                     {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00} // set by library:lora
#define MBED_CONF_PLATFORM_STDIO_CONVERT_NEWLINES          0                                                                                                // set by library:platform
#define MBED_CONF_NORDIC_UART_TIMEOUT_US                   2000                                                                                             // set by library:nordic
#define MBED_CONF_LORA_OVER_THE_AIR_ACTIVATION             1                                                                                                // set by library:lora
#define MBED_CONF_NORDIC_NRF_LF_CLOCK_CALIB_TIMER_INTERVAL 16                                                                                               // set by target:MCU_NRF52832
#define MBED_LFS_ENABLE_INFO                               0                                                                                                // set by library:littlefs
#define MBED_CONF_NORDIC_UART_0_FIFO_SIZE                  32                                                                                               // set by library:nordic
#define MBED_CONF_LORA_PUBLIC_NETWORK                      1                                                                                                // set by library:lora
#define MBED_CONF_EVENTS_SHARED_HIGHPRIO_EVENTSIZE         256                                                                                              // set by library:events
// Macros
#define NRF_BLE_GATT_BLE_OBSERVER_PRIO                     1                                                                                                // defined by library:softdevice
#define NRF_SD_BLE_API_VERSION                             5                                                                                                // defined by library:softdevice
#define NRF_SDH_CLOCK_LF_XTAL_ACCURACY                     7                                                                                                // defined by library:softdevice
#define NRF_SDH_BLE_VS_UUID_COUNT                          4                                                                                                // defined by library:softdevice
#define NRF_SDH_CLOCK_LF_SRC                               1                                                                                                // defined by library:softdevice
#define NRF_SDH_CLOCK_LF_RC_CTIV                           0                                                                                                // defined by library:softdevice
#define BLE_CONN_STATE_BLE_OBSERVER_PRIO                   0                                                                                                // defined by library:softdevice
#define NRF_SDH_BLE_GATT_MAX_MTU_SIZE                      23                                                                                               // defined by library:softdevice
#define NRF_SDH_ENABLED                                    1                                                                                                // defined by library:softdevice
#define NRF_SDH_BLE_SERVICE_CHANGED                        1                                                                                                // defined by library:softdevice
#define NRF_LOG_ENABLED                                    0                                                                                                // defined by library:softdevice
#define NRF_SDH_SOC_ENABLED                                1                                                                                                // defined by library:softdevice
#define NRF_SDH_CLOCK_LF_RC_TEMP_CTIV                      0                                                                                                // defined by library:softdevice
#define NRF_SDH_BLE_GATTS_ATTR_TAB_SIZE                    0x600                                                                                            // defined by library:softdevice
#define NRF_SDH_BLE_GAP_EVENT_LENGTH                       3                                                                                                // defined by library:softdevice
#define NRF_SDH_BLE_OBSERVER_PRIO_LEVELS                   4                                                                                                // defined by library:softdevice
#define UNITY_INCLUDE_CONFIG_H                                                                                                                              // defined by library:utest
#define NRF_SDH_STATE_OBSERVER_PRIO_LEVELS                 2                                                                                                // defined by library:softdevice
#define BLE_CONN_PARAMS_BLE_OBSERVER_PRIO                  1                                                                                                // defined by library:softdevice
#define NRF_SDH_DISPATCH_MODEL                             2                                                                                                // defined by library:softdevice
#define PEER_MANAGER_ENABLED                               1                                                                                                // defined by library:softdevice
#define NRF_SDH_REQ_OBSERVER_PRIO_LEVELS                   2                                                                                                // defined by library:softdevice
#define NRF_SDH_BLE_PERIPHERAL_LINK_COUNT                  3                                                                                                // defined by library:softdevice
#define NRF_SDH_BLE_STACK_OBSERVER_PRIO                    0                                                                                                // defined by library:softdevice
#define BLE_STACK_SUPPORT_REQD                                                                                                                              // defined by library:softdevice
#define FDS_BACKEND                                        2                                                                                                // defined by library:softdevice
#define NRF_SDH_BLE_TOTAL_LINK_COUNT                       4                                                                                                // defined by library:softdevice
#define NRF_SDH_STACK_OBSERVER_PRIO_LEVELS                 2                                                                                                // defined by library:softdevice
#define NRF_SDH_BLE_CENTRAL_LINK_COUNT                     1                                                                                                // defined by library:softdevice
#define SOFTDEVICE_PRESENT                                 1                                                                                                // defined by library:softdevice
#define SWI_DISABLE1                                       1                                                                                                // defined by library:softdevice
#define BLE_ADV_BLE_OBSERVER_PRIO                          1                                                                                                // defined by library:softdevice
#define S132                                                                                                                                                // defined by library:softdevice
#define NRF_SDH_SOC_STACK_OBSERVER_PRIO                    0                                                                                                // defined by library:softdevice
#define NRF_SDH_SOC_OBSERVER_PRIO_LEVELS                   2                                                                                                // defined by library:softdevice
#define NRF_SDH_BLE_ENABLED                                1                                                                                                // defined by library:softdevice

#endif
