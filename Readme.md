# AKM Development Platform

### Important Note

In order to clone the entire firmware project, the `--recurse-submodules` option must be appended to the clone command, as follows:

`git clone <URL> --recurse-submodules`

If you have already cloned the repository without this option and made modifications in the root folder, you will have to clone the individual submodules manually, including the nested submodules of the ECompass library.  To clone them individually, move to each submodule folder and use:

`git submodule update --init`

## Introduction

This is the primary repository for the AKM Development Platform.  The AKM Development Platform (AKDP) interfaces with a PC or smartphone to allow users to experiment and test the functionality of a wide array of products made by AKM Semiconductor, Inc.

The following are instructions on how to set up a build environment to build the AKDP firmware.  If you would just like the firmware driver for a specific device, the drivers are available in individual repositories.  The drivers are not dependent on the AKDP platform and may be incorporated into your own project without modification.

## Getting Started

To compile the firmware, you need to have the GNU ARM Embedded Toolchain Version 6-2017-q2-update or 7-2018-q2-update (https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads).  For merging any combination of the application, bootloader, or softdevice, you need the SRecord utility (http://srecord.sourceforge.net/).

This repository is primarily designed to be used in the Eclipse C/C++ environment.  However, basic project and debug configurations are provided for Microsoft VS Code.  You may also use your own IDE and compile the project with Make (see section "Building with Make" below).

## Building with Make

This repository contains everything necessary to compile the AKDP firmware for all versions of the AKDP hardware (Rev.D7-V1, Rev.D7-V2, and Rev.E).  To build for your particular hardware, provide the appropriate makefile as an option to the "make" command:

`make --file=Makefile.<HW Version>`

where `<HW Version>` is replaced with `V1`, `V2`, or `E`.  Other typical make options may still be appended to the end of the command above (e.g. 'clean', 'all', etc.).

## Debugging with GDB

If you are plannng on debugging directly from the command line or through VS Code, GDB must be able to find the source code files.  To allow GDB to recognize all of the source files, it needs to be able to scan the directory.  Use one of the following commands to allow for source directory scanning in GDB.

*Linux/Cygwin*:
`echo add-auto-load-safe-path $PWD/.gdbinit >> ~/.gdbinit`

*Windows*:
`echo add-auto-load-safe-path $PWD/.gdbinit >> %HOME%/.gdbinit`

Or you may optionally allow GDB to scan all directories on the system:

*Linux/Mac*:
`echo set auto-load safe-path / >> ~/.gdbinit`

*Windows*:
`echo set auto-load safe-path / >> %HOME%/.gdbinit`
