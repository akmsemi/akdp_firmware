/**
 * Firmware for the AKM Development Platform (V1, V2, and Rev.E)
 *
 * All pin definitions and hardware settings are located in akdphwinfo.h
 */
#include <stdio.h>
#include <stdlib.h>
#include "akdphwinfo.h"
#include "Debug.h"
#include "mbed.h"
#include "SerialNano.h"
#include "ble/BLE.h"
#include "ble/services/UARTService.h"
#include "akmsensor.h"
#include "akmsensormanager.h"
#include "akmakd.h"

#define BLE_UUID_TXRX_SERVICE           0x0000 /**< The UUID of the Nordic UART Service. */
#define BLE_UUID_TX_CHARACTERISTIC      0x0002 /**< The UUID of the TX Characteristic. */
#define BLE_UUIDS_RX_CHARACTERISTIC     0x0003 /**< The UUID of the RX Characteristic. */
#define BLE_BUF_LEN                     UARTService::BLE_UART_SERVICE_MAX_DATA_LEN+1
#define TXRX_LEN                        50

BLE                 akdpble;					// DEPRECATED
UARTService*        uartService;
SerialNano*         serial;
AkmSensorManager*   manager;

uint8_t id1;
uint8_t id2;

#ifdef TARGET_RAYTAC_MDBT42Q
DigitalOut led1(DIGITAL_LED1);
DigitalOut led2(DIGITAL_LED2);
DigitalOut led3(DIGITAL_LED3);
DigitalOut led4(DIGITAL_LED4);
#endif

enum {
    UNIT_0_625_MS = 625,
    UNIT_1_25_MS  = 1250,
    UNIT_10_MS    = 10000
};


// default setting of Nexus 5X, Motorola Droid Turbo:
//#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(45, UNIT_1_25_MS)              /**< Minimum connection interval (45 ms) */
//#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(45, UNIT_1_25_MS)               /**< Maximum connection interval (45 ms). */
//#define SLAVE_LATENCY                   0                                             /**< Slave latency. */
//#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(20000, UNIT_10_MS)               /**< Connection supervisory timeout (20 seconds). */

/*
iOS requirement:
Interval Max * (Slave Latency + 1) <= 2 seconds
Interval Min >= 20 ms
Interval Min + 20 ms ≤ Interval Max Slave Latency ≤ 4
connSupervisionTimeout ≤ 6 seconds
Interval Max * (Slave Latency + 1) * 3 < connSupervisionTimeout
*/

#define MSEC_TO_UNITS(TIME, RESOLUTION) (((TIME) * 1000) / (RESOLUTION))
#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(7.5, UNIT_1_25_MS)              /**< Minimum connection interval (7.5 ms) */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(30, UNIT_1_25_MS)               /**< Maximum connection interval (30 ms). */
#define SLAVE_LATENCY                   0                                             /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)               /**< Connection supervisory timeout (4 seconds). */

// Command received from BLE
void WrittenHandler(const GattWriteCallbackParams *params)
{
	DBG_L3("#BLE command received.\r\n");

    static char command[TXRX_LEN]="";
    static uint16_t len=0;
    
    if ((uartService != NULL) && (params->handle == uartService->getTXCharacteristicHandle()))
    {
  //        uint16_t bytesRead = params->len;
  //        char buf[bytesRead];
  //        strncpy(buf, (const char*)params->data, bytesRead);
  //        DBG_L4("#BLE str received: len=%d, str=<%s>\r\n",bytesRead, buf);
        for(uint16_t i = 0; i < params->len; i++)
        {
            if(params->data[i] == CR)
            {
                ;   // ignore CR
            }
            else if(params->data[i] == LF || params->len > TXRX_LEN)
            {
                manager->commandReceived(command);
                memset(command, '\0', TXRX_LEN);
                len = 0;
		        DBG_L2("#BLE command: %s\r\n",command);
            }
            else
            {
                command[len++] = (char)params->data[i];
            }
        }
    }
}

// Command received from USB
static void usbUartCallback(void){

    static char command[TXRX_LEN] = "";
    static uint16_t len=0;

    while(serial->readable())
    {
        uint8_t c = serial->getc();
        
        // ignore CR
        if(c != CR)
        {
            command[len++] = c;
            if(len>=TXRX_LEN || c == LF)
            {
                manager->commandReceived(command);
                memset(command, '\0', TXRX_LEN);
                len = 0;
            }
        }
    }
}

/**
 * Callback for successful BLE connection
 */
static void connectionCallback(const Gap::ConnectionCallbackParams_t *params)
{
    DBG_L3("#From central: minConnectionInterval = %d\r\n", params->connectionParams->minConnectionInterval);
    DBG_L3("#From central: maxConnectionInterval = %d\r\n", params->connectionParams->maxConnectionInterval);
    DBG_L3("#From central: slaveLatency = %d\r\n", params->connectionParams->slaveLatency);
    DBG_L3("#From central: connectionSupervisionTimeout = %d\r\n", params->connectionParams->connectionSupervisionTimeout);
    
    Gap::Handle_t gap_handle = params->handle;
    Gap::ConnectionParams_t gap_conn_params;
    gap_conn_params.minConnectionInterval = params->connectionParams->minConnectionInterval;
    gap_conn_params.maxConnectionInterval = params->connectionParams->maxConnectionInterval;
    gap_conn_params.slaveLatency = params->connectionParams->slaveLatency;
    gap_conn_params.connectionSupervisionTimeout = CONN_SUP_TIMEOUT;
    akdpble.updateConnectionParams(gap_handle, &gap_conn_params);		// DEPRECATED

    DBG_L3("#From peripheral: minConnectionInterval = %d\r\n", gap_conn_params.minConnectionInterval);
    DBG_L3("#From peripheral: maxConnectionInterval = %d\r\n", gap_conn_params.maxConnectionInterval);
    DBG_L3("#From peripheral: slaveLatency = %d\r\n", gap_conn_params.slaveLatency);
    DBG_L3("#From peripheral: connectionSupervisionTimeout = %d\r\n", gap_conn_params.connectionSupervisionTimeout);

    manager->setEventConnected();
    DBG_L3("#BLE Connected\r\n");

#ifdef TARGET_RAYTAC_MDBT42Q
    led1.write(0);
    led2.write(1);				// Change LEDs when BLE is connected
#endif
}

/**
 * Callback upon BLE disconnection
 */
static void disconnectionCallback(const Gap::DisconnectionCallbackParams_t *params)
{
    manager->setEventDisconnected();
    DBG_L3("#BLE Disconnected\r\n");

#ifdef TARGET_RAYTAC_MDBT42Q
    led1.write(1);
    led2.write(0);
#endif

    akdpble.gap().startAdvertising();
} 

/**
 * BLE initialization
 */
void bleSetup(const char* device_name){
    akdpble.init();

    // setup advertising 
    akdpble.gap().accumulateAdvertisingPayload(GapAdvertisingData::BREDR_NOT_SUPPORTED);
    akdpble.gap().setAdvertisingType(GapAdvertisingParams::ADV_CONNECTABLE_UNDIRECTED);
    akdpble.gap().accumulateAdvertisingPayload(GapAdvertisingData::SHORTENED_LOCAL_NAME,
                                    (const uint8_t *)device_name, strlen(device_name));
//                                    (const uint8_t *)DEVICE_NAME, sizeof(DEVICE_NAME) - 1);
    akdpble.gap().accumulateAdvertisingPayload(GapAdvertisingData::COMPLETE_LIST_128BIT_SERVICE_IDS,
                                    (const uint8_t *)UARTServiceUUID_reversed, sizeof(UARTServiceUUID_reversed));

    
    // Set desired connection parameters
    Gap::ConnectionParams_t gap_conn_params;
    gap_conn_params.minConnectionInterval = MIN_CONN_INTERVAL;
    gap_conn_params.maxConnectionInterval = MAX_CONN_INTERVAL;
    gap_conn_params.slaveLatency = SLAVE_LATENCY;
    gap_conn_params.connectionSupervisionTimeout = CONN_SUP_TIMEOUT;
    akdpble.setPreferredConnectionParams(&gap_conn_params);				// DEPRECATED
    
    akdpble.gap().onDisconnection(disconnectionCallback);
    akdpble.gap().onConnection(connectionCallback);
    akdpble.gattServer().onDataWritten(WrittenHandler);  
    
    akdpble.gap().setAdvertisingInterval(160);	// 100ms; in multiples of 0.625ms
    akdpble.gap().startAdvertising(); 
}

/**
 * Initialize the AKDP by setting up I2C, SPI, and sampling the ID pins
 */
bool initAkdpBoard(){
    DBG_L3("#Initializing AKDP...\r\n");

	uint8_t subid_bitlen;

#ifdef TARGET_RAYTAC_MDBT42Q
	DigitalOut _nrst(NRST);

    //  Read ID1 from ADC
    id1 = AkmSensorManager::getId(ANALOG_IN_ID1, 4);

    // 5bit sub ID categories
    if( id1 == AkmSensor::AKM_PRIMARY_ID_AKD_SPI || id1 == AkmSensor::AKM_PRIMARY_ID_AKD_I2C ||
        id1 == AkmSensor::AKM_PRIMARY_ID_TBD1 || id1 == AkmSensor::AKM_PRIMARY_ID_CURRENT_SENSOR_3V ||
        id1 == AkmSensor::AKM_PRIMARY_ID_MOTOR_DRIVER )
    {
//        DBG_L2("#ID2 bit length: 5\r\n");
        subid_bitlen = 5;
    }
    else{
//    	DBG_L2("#ID2 bit length: 4\r\n");
    	subid_bitlen = 4;
	}

    id2 = AkmSensorManager::getId(ANALOG_IN_ID2, subid_bitlen);

////////////////// TEST
//    if(id1 == AkmSensor::AKM_PRIMARY_ID_AKD_I2C && id2 == 0x04){
//    	id2 = 0x03;
//    }
////////////////// TEST

    if( (id1 == 11 && id2 == 11) || (id1 == 55 && id2 == 55) ){
        return true;
    }

    // RSTN control
    if(id1 == AkmSensor::AKM_PRIMARY_ID_AKD_SPI || id1 == AkmSensor::AKM_PRIMARY_ID_AKD_I2C){
		_nrst.write(0);
        wait_ms(100);
		_nrst.write(1);
        DBG_L2("#Detect AKD, RSTN control.\r\n");
    }

#endif // TARGET_RAYTAC_MDBT42Q

#if defined(TARGET_RBLAB_BLENANO) || defined(TARGET_RBLAB_BLENANO2)
    // CSN High to activate I2C_GATE
    DigitalOut _cs = DigitalOut(SPI_CS);
    _cs.write(1);

    // I2C communication ports to HIGH(just in case).
    DigitalOut _scl = DigitalOut(I2C_SCL);
    _scl.write(1);
    DigitalOut _sda = DigitalOut(I2C_SDA);
    _sda.write(1);
    
    // MISO set to HIGH
//    DigitalOut _miso = DigitalOut(SPI_MISO);
//    _miso.write(1);

    DBG_L2("#SCL,SDA port high.\r\n");
    wait_ms(TIME_FOR_OE_MS);
    
    const TCA9554A::Port PORT_OE_LVS1   = TCA9554A::PORT_7;
    const TCA9554A::Port PORT_OE_LVS2   = TCA9554A::PORT_6;
    const TCA9554A::Port PORT_SPIN      = TCA9554A::PORT_5;
    const TCA9554A::Port PORT_RSV_RSTN  = TCA9554A::PORT_0;

    I2C i2c(I2C_SDA, I2C_SCL);
    
    // call I2C general reset only once
    char cmd[] = {0x06};    // general reset code
    i2c.write(0x00, cmd, 1);
    DBG_L2("#General Reset.\r\n");
    wait_ms(TIME_FOR_OE_MS);
    
    TCA9554A tca9554a(&i2c, TCA9554A::SLAVE_ADDRESS_38H);
    
    // Initializes TCA9554A (I2C GPIO Expander)
    tca9554a.configurePort(PORT_OE_LVS1, TCA9554A::DIR_OUTPUT);
    tca9554a.configurePort(PORT_OE_LVS2, TCA9554A::DIR_OUTPUT);
    tca9554a.configurePort(PORT_SPIN, TCA9554A::DIR_OUTPUT);
    tca9554a.configurePort(PORT_RSV_RSTN, TCA9554A::DIR_OUTPUT);
    
    //  enable LVS1 and LVS2 level shifter
    tca9554a.setPortLevel(PORT_OE_LVS1, TCA9554A::HIGH);
    tca9554a.setPortLevel(PORT_OE_LVS2, TCA9554A::HIGH);
    tca9554a.setPortLevel(PORT_RSV_RSTN, TCA9554A::HIGH);
    tca9554a.setPortLevel(PORT_SPIN, TCA9554A::HIGH);
    wait_ms(TIME_FOR_OE_MS);

    //  reset LVS2
    tca9554a.setPortLevel(PORT_OE_LVS2, TCA9554A::LOW);
    wait_ms(TIME_FOR_OE_MS);
    tca9554a.setPortLevel(PORT_OE_LVS2, TCA9554A::HIGH);
    wait_ms(TIME_FOR_OE_MS);

    //  reset LVS1
    tca9554a.setPortLevel(PORT_OE_LVS1, TCA9554A::LOW);
    wait_ms(TIME_FOR_OE_MS);
    tca9554a.setPortLevel(PORT_OE_LVS1, TCA9554A::HIGH);
    wait_ms(TIME_FOR_OE_MS);

    //  disable LVS1 level shifter to read ID
    tca9554a.setPortLevel(PORT_OE_LVS1, TCA9554A::LOW);
    wait_ms(TIME_FOR_OE_MS);
    
    //  read ID and subId from ADC
    DBG_L3("#read ID and subId from ADC.\r\n");
    //id1 = AkmSensorManager::getId(&i2c, ANALOG_IN_ID1, 4);
    id1 = AkmSensorManager::getId(ANALOG_IN_ID1, 4);
    subid_bitlen = 4;
    // 5bit sub ID categories
    if( id1 == AkmSensor::AKM_PRIMARY_ID_AKD_SPI || 
        id1 == AkmSensor::AKM_PRIMARY_ID_AKD_I2C || 
        id1 == AkmSensor::AKM_PRIMARY_ID_TBD1 || 
        id1 == AkmSensor::AKM_PRIMARY_ID_CURRENT_SENSOR_3V || 
        id1 == AkmSensor::AKM_PRIMARY_ID_MOTOR_DRIVER ){
//        DBG_L3("#5 bit sub ID.\r\n");
        subid_bitlen = 5;
    }
    // id2 = AkmSensorManager::getId(&i2c, ANALOG_IN_ID2, subid_bitlen);
    id2 = AkmSensorManager::getId(ANALOG_IN_ID2, subid_bitlen);
    DBG_L2("#ID=%d subID=%d.\r\n", id1, id2);

    if( (id1 == 11 && id2 == 11) || (id1 == 55 && id2 == 55) ){
        return true;
    }

    //  enable 1.8V level shifter
    tca9554a.setPortLevel(PORT_OE_LVS1, TCA9554A::HIGH);
    DBG_L2("#LVS1 High.\r\n");
    wait_ms(TIME_FOR_OE_MS);

    // RSTN control
    if(id1 == AkmSensor::AKM_PRIMARY_ID_AKD_SPI || id1 == AkmSensor::AKM_PRIMARY_ID_AKD_I2C){
        tca9554a.setPortLevel(PORT_RSV_RSTN, TCA9554A::LOW);
        wait_ms(TIME_FOR_OE_MS);
        tca9554a.setPortLevel(PORT_RSV_RSTN, TCA9554A::HIGH);                
        DBG_L2("#Detect AKD, RSTN control.\r\n");
    }

    // SPI disable/enable
    if( id1 == AkmSensor::AKM_PRIMARY_ID_AKD_SPI || id1 == AkmSensor::AKM_PRIMARY_ID_ANGLE_SENSOR ){
        tca9554a.setPortLevel(PORT_SPIN, TCA9554A::LOW);
        // Disable 5.0V level shifter in order to ADC doesn't respond.
        tca9554a.setPortLevel(PORT_OE_LVS2, TCA9554A::LOW);
        DBG_L2("#Detect SPI, set SPIN low.\r\n");
    }
    else{
        tca9554a.setPortLevel(PORT_SPIN, TCA9554A::HIGH);
        tca9554a.setPortLevel(PORT_OE_LVS2, TCA9554A::HIGH);
    }

#endif // defined(TARGET_RBLAB_BLENANO) || defined(TARGET_RBLAB_BLENANO2)

    wait_ms(TIME_FOR_OE_MS);

    // Disable TWI
	NRF_TWI0->ENABLE = TWI_ENABLE_ENABLE_Disabled << TWI_ENABLE_ENABLE_Pos;
	NRF_TWI1->ENABLE = TWI_ENABLE_ENABLE_Disabled << TWI_ENABLE_ENABLE_Pos;

#ifdef TARGET_RBLAB_BLENANO
	NRF_TWI0->POWER  = 0;
  	NRF_TWI1->POWER  = 0;
#endif
    
    return false;
}

char* str_concat(char* str1, char* str2)
{
    int num1;
    char* str;
    
    num1=strlen(str1) + strlen(str2);
    str = (char *)malloc(num1 + 1);
    sprintf(str,"%s%s",str1,str2);
    return str;
}

void error(const char* format, ...) {
}

int main(void)
{

    serial = new SerialNano(USB_TX, USB_RX);

#ifdef DEBUG
    Debug::setSerial(serial);
#endif

    // serial port RX event
    serial->attach(&usbUartCallback);

//#if defined(TARGET_RAYTAC_MDBT42Q) || defined(TARGET_RBLAB_BLENANO2)
    wait_ms(5000);		// BLE Nano 2 needs to wait before outputting to serial
//#endif

#ifdef TARGET_RBLAB_BLENANO
    serial->printf("#AKDP D7-V1 Connected.\r\n");
#elif defined(TARGET_RBLAB_BLENANO2)
   	serial->printf("#AKDP D7-V2 Connected.\r\n");
#elif defined(TARGET_RAYTAC_MDBT42Q)
	serial->printf("#AKDP E2 Connected.\r\n");
#else
	serial->printf("#Error: Unable to determine motherboard.\r\n");
#endif

    // Initialize AKDP board
    if( initAkdpBoard() ){
        DBG_L1("#Error: AKDP boot failed.\r\n");
    }

#ifdef TARGET_RAYTAC_MDBT42Q
    led1.write(1);				// Board initialized indicator
#endif

    // create sensor manager
    manager = new AkmSensorManager(serial);
    
    if( manager->init(id1, id2) == AkmSensorManager::ERROR){
        DBG_L1("#Error: main() - Sensor not detected.\r\n");
    }
    
    // create device name
    // "AKDP BB.FF SENSOR_NAME" (BB = board version, FF = firmware version)
    char fw_version[5];
    sprintf(fw_version, "%03d", FIRMWARE_VERSION);
    char* name = str_concat(MOTHER_BOARD_NAME, " ");
    name = str_concat(name, MOTHER_BOARD_VERSION);
    name = str_concat(name, ".");
    name = str_concat(name, fw_version);
    name = str_concat(name, " ");
    name = str_concat(name, manager->getSensorName());
    
    // Initialize BLE interface
    bleSetup(name);
    uartService = new UARTService(akdpble);
    manager->setBleUartService(uartService);
	
    DBG_L3("#BLE Initialized\r\n");

    // main loop
    while(1)
    {   		
        if(manager->isEvent()){
            manager->processEvent();
        }else{
            akdpble.waitForEvent();						// DEPRECATED
        }
    }
}

