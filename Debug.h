#ifndef DEBUG_H
#define DEBUG_H

#include "akdphwinfo.h"

#ifdef DEBUG

#include "mbed.h"
#include "SerialNano.h"


class Debug {
public:
    static void setSerial(SerialNano *p);
    static SerialNano* getSerial();

private:
    static SerialNano *serial;
};

#endif // DEBUG


#if defined(DEBUG_LEVEL1) || defined(DEBUG)
#define DBG_L1(...) if(Debug::getSerial()!=NULL)Debug::getSerial()->printf(__VA_ARGS__)
#else
#define DBG_L1(...)
#endif // DEBUG_LEVEL1

#ifdef DEBUG_LEVEL4
#define DBG_L4(...) if(Debug::getSerial()!=NULL)Debug::getSerial()->printf(__VA_ARGS__)
#define DEBUG_LEVEL3
#else
#define DBG_L4(...)
#endif // DEBUG_LEVEL4

#ifdef DEBUG_LEVEL3
#define DBG_L3(...) if(Debug::getSerial()!=NULL)Debug::getSerial()->printf(__VA_ARGS__)
#define DEBUG_LEVEL2
#else
#define DBG_L3(...)
#endif // DEBUG_LEVEL3

#ifdef DEBUG_LEVEL2
#define DBG_L2(...) if(Debug::getSerial()!=NULL)Debug::getSerial()->printf(__VA_ARGS__)
#else
#define DBG_L2(...)
#endif // DEBUG_LEVEL2


#endif // DEBUG_H

