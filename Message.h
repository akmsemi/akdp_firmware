#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#define MESSAGE_MAX_ARG		32
#define MESSAGE_MAX_LEN		1 + 2 + 2*MESSAGE_MAX_ARG + 1 + 1	// '$' + CMD + DATA + '\r' + '\n'

/**
 * Takes messages received from BLE or USB as ASCII and converts them to
 * char format.  Extracts the command and arguments from the messages and
 * stores them.
 *
 * The list of possible commands is enumerated below.
 */
class Message {
public:
	typedef enum {
        CMD_GET_HW_VERSION = 0x01,                     /**< Gets HW version.                */
        CMD_GET_MAG_PART = 0x02,                       /**< Gets magnetic sensor ID         */
        CMD_BOOT_STATUS = 0x0F,                        /**< Boot status.         */
        CMD_GET_SENSOR_INDEX = 0x06,                   /**< Get the sensor index.         */
        CMD_SET_SENSOR_INDEX = 0x07,                   /**< .Set the sensor index.         */
        CMD_GET_TOTAL_SENSOR_NUM = 0x08,               /**< Get the total number of sensors.         */
        
        CMD_MOTOR_START_MOTOR = 0x50,                   /**< Start motor(s). */
        CMD_MOTOR_STOP_MOTOR = 0x51,                    /**< Stop motor(s). */
        CMD_MOTOR_SET_DIRECTION = 0x52,                 /**< Set direction of motor(s). */
        CMD_MOTOR_SET_DUTY_CYCLE = 0x53,                /**< Set duty cycle of motor. */

//        CMD_GET_THRESHOLD = 0x80,                      /**< Gets threshold of AK0997x       */
//        CMD_SET_THRESHOLD_FROM_HOST = 0x81,            /**< Sets threshold of AK0997x from host. */
//        CMD_SET_MEASUREMENT_FREQUENCY = 0x82,          /**< Sets measurement frequency.     */
//        CMD_SEND_SWITCH_MODE_MEASUREMENT_DATA = 0x83,  /**< Sends measurement data on/off   */
//        CMD_SWITCH_MODE_MEASUREMENT_DATA = 0x84,       /**< Measurement data in switch mode */
//        CMD_GET_POLARITY = 0x85,                       /**< Gets polarity of AK0997x        */
//        CMD_SET_POLARITY = 0x86,                       /**< Sets polarity of AK0997x        */
        CMD_PROGSW_GET_THRESHOLD = 0x80,               /**< Gets threshold of AK09970.       */
        CMD_PROGSW_SET_THRESHOLD = 0x81,               /**< Sets threshold of AK09970.       */
//        CMD_PROGSW_GET_READ_ADDRESS = 0x82,            /**< Gets read address of AK09970.       */
//        CMD_PROGSW_SET_READ_ADDRESS = 0x83,            /**< Sets read address of AK09970.       */
//        CMD_PROGSW_GET_SWITCH_MODE = 0x84,             /**< Gets interrupt mode of AK09970.       */
//        CMD_PROGSW_SET_SWITCH_MODE = 0x85,             /**< Sets interrupt mode of AK09970.       */
        CMD_PROGSW_GET_READ_COFIGURATION = 0x82,		/**< Gets read configuration of AK09970.       */
        CMD_PROGSW_SET_READ_COFIGURATION = 0x83,		/**< Sets read configuration of AK09970.	*/
        CMD_PROGSW_GET_SWITCH_COFIGURATION = 0x84,		/**< Gets switch configuration of AK09970.	*/
        CMD_PROGSW_SET_SWITCH_COFIGURATION = 0x85,		/**< Sets switch configuration of AK09970.	*/
        CMD_PROGSW_GET_OPERATION_MODE = 0x86,			/**< Gets operation mode of AK09970.		*/
        CMD_PROGSW_SET_OPERATION_MODE = 0x87,			/**< Sets operation mode of AK09970.		*/
		CMD_PROGSW_GET_CONTROL_MODE = 0x88,				/**< Gets the control mode of the AK09971.	*/
		CMD_PROGSW_SET_CONTROL_MODE = 0x89,				/**< Sets the control mode of the AK09971.	*/
		CMD_PROGSW_WRITE_EEPROM = 0x90,					/**< Writes the current settings to EEPROM.	*/

        CMD_IR_GET_THRESHOLD = 0x90,                   /**< Gets threshold of AK9750.       */
        CMD_IR_SET_THRESHOLD = 0x91,                   /**< Sets threshold of AK9750.       */
        CMD_IR_GET_HYSTERESIS = 0x92,                  /**< Gets hysteresis setting of AK9750 */
        CMD_IR_SET_HYSTERESIS = 0x93,                  /**< Sets hysteresis to AK9750       */
        CMD_IR_GET_INTERRUPT = 0x94,                   /**< Gets interrupt mode of AK9750   */
        CMD_IR_SET_INTERRUPT = 0x95,                   /**< Sets interrupt mode of AK9750   */
        CMD_IR_GET_OPERATION_MODE = 0x96,              /**< Gets operation mode of AK9750   */
        CMD_IR_SET_OPERATION_MODE = 0x97,              /**< Sets operation mode of AK9750   */
        CMD_IR_GET_THRESHOLD_EEPROM = 0x98,            /**< Gets EEPROM threshold of AK9750.       */
        CMD_IR_SET_THRESHOLD_EEPROM = 0x99,            /**< Sets EEPROM threshold of AK9750.       */
        CMD_IR_GET_HYSTERESIS_EEPROM = 0x9A,           /**< Gets EEPROM hysteresis setting of AK9750 */
        CMD_IR_SET_HYSTERESIS_EEPROM = 0x9B,           /**< Sets EEPROM hysteresis to AK9750       */
        CMD_IR_GET_INTERRUPT_EEPROM = 0x9C,            /**< Gets EEPROM interrupt mode of AK9750   */
        CMD_IR_SET_INTERRUPT_EEPROM = 0x9D,            /**< Sets EEPROM interrupt mode of AK9750   */
        CMD_IR_GET_OPERATION_MODE_EEPROM = 0x9E,       /**< Gets EEPROM operation mode of AK9750   */
        CMD_IR_SET_OPERATION_MODE_EEPROM = 0x9F,       /**< Sets EEPROM operation mode of AK9750   */

        CMD_DOOR_STATE_CHANGED = 0xA0,                 /**< Door state changed.             */
        CMD_UPDATE_MAG_VECTOR = 0xA1,                  /**< Update magnetic vector state.   */
        CMD_THRESHOLD_UPDATED = 0xA2,                  /**< Threshold updated.              */
        CMD_GET_CURRENT_DOOR_STATE = 0xA3,             /**< Gets the current door state.    */
        CMD_IR_STATE_CHANGED = 0xA5,                   /**< IR state changed.               */
        CMD_UPDATE_IR_STATE = 0xA6,                    /**< Update IR state 1 or 2          */
        CMD_GET_CURRENT_HUMAN_PRESENCE_STATE = 0xA7,   /**< Gets the current human presence state. */
        CMD_IR_THRESHOLD_UPDATED = 0xA8,               /**< IR threshold updated.           */

//        CMD_MOTION_DETECTED = 0xB1,                    /**< Motion detected.                */
//        CMD_GET_MOTION_THRESHOLD = 0xB2,               /**< Get motion threshold            */
//        CMD_SET_MOTION_THRESHOLD = 0xB3,               /**< Set motion threshold            */
        CMD_UNKNOWN = 0x00,                            /**< Unknown command.                */
        CMD_SET_SERIAL_TARGET = 0x0A,                  /**< Set serial output target        */
        CMD_REG_WRITE = 0x11,                          /**< Register write 1 byte           */
        CMD_REG_WRITEN = 0x12,                         /**< Register write N bytes          */
        CMD_REG_READ = 0x13,                           /**< Register read 1 byte            */
        CMD_REG_READN = 0x14,                          /**< Register read N bytes           */
        CMD_COMPASS_GET_OPERATION_MODE = 0x16,         /**< Gets operation mode of e-compass*/
        CMD_COMPASS_SET_OPERATION_MODE = 0x17,         /**< Sets operation mode of e-compass*/
        CMD_GET_ID = 0xB0,                             /**< Get connected sensor ID         */
        CMD_STOP_MEASUREMENT = 0xBA,                   /**< Stop Measurement                */
        CMD_START_MEASUREMENT = 0xBB,                  /**< Start Measurement               */
        CMD_ANGLE_ZERO_RESET = 0xBC,                   /**< Angle sensor zero reset         */
        CMD_ANGLE_READ = 0xBD,                         /**< Angle sensor read angle         */
    } Command;

	typedef enum{
		SW_OFF = 0x00,
		SW_ON = 0x01,
	} SwitchState;

    typedef enum {
        SUCCESS = 0,
        END_OF_STR,
        ERROR_ILLEGAL_CHAR,
        ERROR_INDEX_OUT_OF_BOUNDS,
    } Status;
    
    /**
     * Constructor.
     */
    Message() : cmd(CMD_UNKNOWN), num_arg(0){
    };
    
    /**
     * Convert an ascii into one byte value.
     * @param out Pointer to the buffer stores result value
     * @param in Pointer to a hex format ascii value. The value must be expressed by two-byte ascii chars. For example, the value 5 must be expressed as "05", not "5".
     */   
    static Status asciiToChar(char *out, const char *in);
    
    /**
     * Convert a byte value into a hex format ascii value. The result value have two-byte length. If the input value is 5, the result string is "05", not "5".
     * @param out Pointer to the buffer stores result ascii. The buffer must have two byte length.
     * @param in Pointer to a byte value to be converted.
     */
    static void charToAscii(char *out, const char *in);

    /**
     * Parses given string and construct a message object.
     * @param msg Pointer to an message object.
     * @param str Pointer to an string to be parsed.
     * @return Returns SUCCESS if parsed successfully, otherwise returns other code.
     */
    static Status parse(Message *msg, const char *str);
    
    /**
     * Sets the command field of the message.
     * @param cmd Command to be set.
     */
    void setCommand(Command cmd);
    
    /**
     * Sets the argument(s) of the message.
     * @param arg pointer to the argument(s) to be stored.
     * @param len length of the argument in bytes
     */
    void setArguments(const char *arg, int len);
    
    /**
     * Sets argument at the specified index.
     * @param arg value of the argument
     */
    void setArgument(int index, char arg);
    
    /**
     * Gets the command contained in the message.
     * @return The command contained in the message.
     */
    Command getCommand() const;
    
    /**
     * Gets the argument at location n=index in the message.
     * @param index Argument number. (0 <= index <= getArgNum())
     * @return Returns value of the argument specified by index. Returns 0 if
     *         the index is out of bounds.
     */
    char getArgument(int index) const;
    
    /**
     * Gets number of arguments in command.
     * @return Returns number of arguments contained in the message.
     */
    int getArgNum() const;
    
    /**
     * Gets arguments.
     * @param buf String for arguments to be stored.
     * @param maxNum Maximum number of arguments to be read.
     */
    void getArguments(char *buf, int maxNum) const;

        
private:
//    const static int NUM_MAX_ARG = 32;                           /**< Maximum number of arguments in message. */
//    const static int MAX_LEN = 1 + 2 + 2*NUM_MAX_ARG + 1 + 1;    /**< Maximum length of message in byte unit. $ + command(2bytes) + data + \r + \n */

    Command		cmd;					/**< Holds command. */
    char		arg[MESSAGE_MAX_ARG];	/**< Holds arguments. */
    int			num_arg;				/**< Holds number of arguments. */
};

#endif // __MESSAGE_H__

