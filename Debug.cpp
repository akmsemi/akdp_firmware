#include "Debug.h"

#ifdef DEBUG

SerialNano *Debug::serial = NULL;

void Debug::setSerial(SerialNano *p) {
    serial = p;
}

SerialNano* Debug::getSerial() {
    return serial;
}

#endif // DEBUG
