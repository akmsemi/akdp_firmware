#ifndef __AKDPHWINFO_H__
#define __AKDPHWINFO_H__

//#define DEBUG
//#define DEBUG_LEVEL4

#define MOTHER_BOARD_NAME               "AKDP"
#define CR                              '\r'
#define LF                              '\n'

#ifdef DEBUG
#define FIRMWARE_VERSION				00
#else
#define FIRMWARE_VERSION                29
#endif // DEBUG

/*
 * Definitions for AKDP Rev.E
 */
#ifdef TARGET_RAYTAC_MDBT42Q

#define MOTHER_BOARD_VERSION            "E2"
#define BOARD_VERSION                   2
#define HARDWARE_VERSION                3

// AKDP Rev.E: Protocol Settings
#define UART_BAUD_RATE                  921600     /**< UART Baud Rate */
#define I2C_SPEED                       400000     /**< I2C Speed */
#define SPI_SPEED                       4000000    /**< SPI Speed */
#define SENSOR_SAMPLING_RATE            0.1       // 100Hz
#define TIME_FOR_OE_MS                  100

 // AKDP Rev.E: UART Pin Definitions
#define USB_TX                          P0_7
#define USB_RX                          P0_6
// AKDP Rev.E:  SPI Pin Number Definitions
#define SPI_SCK                         P0_31   // SPI, SCK (shared with D3)
#define SPI_MISO                        P0_29   // SPI, MISO (shared with D1)
#define SPI_MOSI                        P0_30   // SPI, MOSI (shared with D2)
#define SPI_CS                          P0_28   // SPI, CS (shared with D0)
#define SPI_DRDY                        P0_8   // SPI, DRDY (shared with D5)
// AKDP Rev.E:  I2C Pin Number Definitions
#define I2C_SCL                         P0_31   // I2C, SCL (shared with D3)
#define I2C_SDA                         P0_30   // I2C, SDA (shared with D2)
#define I2C_DRDY                        P0_28   // I2C, DRDY (shared with D0)
// AKDP Rev.E:  Digital Port Pin Number Definitions
#define DIGITAL_D0                      P0_28
#define DIGITAL_D1                      P0_29
#define DIGITAL_D2                      P0_30
#define DIGITAL_D3                      P0_31
#define DIGITAL_D4                      P0_8
#define DIGITAL_D5                      P0_11
#define DIGITAL_D6                      P0_12
#define DIGITAL_D7                      P0_13
#define DIGITAL_D8                      P0_17
#define DIGITAL_D9                      P0_19
#define DIGITAL_D10                     P0_9
#define DIGITAL_D11                     P0_10
#define DIGITAL_D12                     P0_14
#define DIGITAL_D13                     P0_15
#define DIGITAL_D14                     P0_16
#define DIGITAL_D15                     P0_20
#define DIGITAL_LED1                    P0_22
#define DIGITAL_LED2                    P0_25
#define DIGITAL_LED3                    P0_26
#define DIGITAL_LED4                    P0_27
// AKDP Rev.E:  Analog In Pin Number Definitions
#define ANALOG_IN_ID1                   P0_2
#define ANALOG_IN_ID2                   P0_3
#define ANALOG_BDETECT                  P0_4
#define ANALOG_IN_A0                    P0_5       // Both 3V and 5V devices
// AKDP Rev.E:  Reset Definition
#define NRST                            P0_19

#endif // TARGET_RAYTAC_MDBT42Q


/*
 * Definitions for AKDP Rev.D7 V2
 */
#ifdef TARGET_RBLAB_BLENANO2

#include "tca9554a.h"
#include "mcp342x.h"

#define MOTHER_BOARD_VERSION            "D7-V2"
#define BOARD_VERSION                   1
#define HARDWARE_VERSION                2

// AKDP Rev.D7 V2: Protocol Settings
#define UART_BAUD_RATE                  921600     /**< UART Baud Rate */
#define I2C_SPEED                       400000     /**< I2C Speed */
#define SPI_SPEED                       1000000    /**< SPI Speed */
#define SENSOR_SAMPLING_RATE		0.1	   /**< Analog Sampling Rate */
#define TIME_FOR_OE_MS                  100

// AKDP Rev.D7 V2:  Serial Pin Number Definitions
#define USB_TX                          P0_5       // Pin A4
#define USB_RX                          P0_4       // Pin A5
// AKDP Rev.D7 V2:  SPI Pin Number Definitions
#define SPI_SCK                         P0_2       // SPI1_SCK
#define SPI_MISO                        P0_29      // SPI1_MISO
#define SPI_MOSI                        P0_28      // SPI1_MOSI
#define SPI_CS                          P0_30      // SPI1_SS
#define SPI_DRDY                        P0_7       // SPI DRDY (Pin D10)
// AKDP Rev.D7 V2:  I2C Pin Number Definitions
#define I2C_SCL                         P0_2       // SCL0
#define I2C_SDA                         P0_28      // SDA0
#define I2C_DRDY                        P0_30      // Pin D0
// AKDP Rev.D7 V2:  Digital Port Pin Number Definitions
#define DIGITAL_D0                      P0_30      // Pin D0
#define DIGITAL_D1                      P0_29      // Pin D1
#define DIGITAL_D2                      P0_8
#define DIGITAL_D3                      P0_2
#define DIGITAL_D4                      P0_5
#define DIGITAL_D5                      P0_4
#define DIGITAL_D6                      P0_3
#define DIGITAL_D7                      P0_6
// AKDP Rev.D7 V2:  Analog In Pin Number Definitions
#define ANALOG_IN_PIN                   P0_4       // Pin A5
// AKDP Rev.D7 V2:  Analog Sensors ID Definitions
#define ANALOG_IN_ID1                   P0_5       // Pin A4
#define ANALOG_IN_ID2                   P0_4       // Pin A5

#endif // TARGET_RBLAB_BLENANO2


/*
 * Definitions for AKDP Rev.D7 V1
 */
#ifdef TARGET_RBLAB_BLENANO

#include "tca9554a.h"
#include "mcp342x.h"

#define MOTHER_BOARD_VERSION            "D7-V1"
#define BOARD_VERSION                   1
#define HARDWARE_VERSION                1

// AKDP Rev.D7 V1: Protocal Settings
#define UART_BAUD_RATE                  115200     /**< UART Baud Rate */
#define I2C_SPEED                       100000     /**< I2C Speed */
#define SPI_SPEED                       1000000    /**< SPI Speed */
#define SENSOR_SAMPLING_RATE		0.1	   /**< Analog Sensor Sampling Rate */
#define TIME_FOR_OE_MS                  100

// AKDP Rev.D7 V1:  Serial Pin Number Definitions
#define USB_TX                          P0_4       // Pin A3
#define USB_RX                          P0_5       // Pin A4
// AKDP Rev.D7 V1:  SPI Pin Number Definitions
#define SPI_SCK                         P0_8       // SPI, SCK
#define SPI_MISO                        P0_9       // SPI, MISO
#define SPI_MOSI                        P0_10      // SPI, MOSI
#define SPI_CS                          P0_11      // SPI, CS
#define SPI_DRDY                        P0_7       // SPI, DRDY
// AKDP Rev.D7 V1:  I2C Pin Number Definitions
#define I2C_SCL                         P0_8       // I2C, SCL
#define I2C_SDA                         P0_10      // I2C, SDA
// AKDP Rev.D7 V1:  Digital Port Pin Number Definitions
#define DIGITAL_D0                      P0_11      // Pin D0
#define DIGITAL_D1                      P0_9       // Pin D1
#define DIGITAL_D3                      P0_8
#define DIGITAL_D4                      P0_28
#define DIGITAL_D5                      P0_29
#define DIGITAL_D6                      P0_15
#define DIGITAL_D7                      P0_7
// AKDP Rev.D7 V1:  DRDY Port Pin Number Definitions
#define I2C_DRDY                        P0_11      // Pin D0
// AKDP Rev.D7 V1:  Analog In Pin Number Definitions
#define ANALOG_IN_PIN                   P0_5       // Pin A4
// AKDP Rev.D7 V1:  Analog Sensors ID Definitions
#define ANALOG_IN_ID1                   P0_4
#define ANALOG_IN_ID2                   P0_5

#endif // TARGET_RBLAB_BLENANO


#endif // __AKDPHWINFO_H__
